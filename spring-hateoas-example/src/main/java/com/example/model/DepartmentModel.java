package com.example.model;


import org.springframework.hateoas.RepresentationModel;

import java.util.List;

public class DepartmentModel extends RepresentationModel<DepartmentModel> {
    private int id;
    private String name;

    public DepartmentModel() {
    }

    public DepartmentModel(int id, String name, List<EmployeeModel> employees) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "DepartmentModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
