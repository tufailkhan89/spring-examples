package com.example.model.assembler;

import com.example.controller.EmployeeController;
import com.example.entity.EmployeeEntity;
import com.example.model.EmployeeModel;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class EmployeeModelAssembler extends RepresentationModelAssemblerSupport<EmployeeEntity, EmployeeModel> {

    public EmployeeModelAssembler() {
        super(EmployeeController.class, EmployeeModel.class);
    }

    @Override
    public CollectionModel toCollectionModel(Iterable<? extends EmployeeEntity> entities) {
        CollectionModel<EmployeeModel> employees = super.toCollectionModel(entities);

//        employees.forEach(employee -> {
//            Link selfLink = WebMvcLinkBuilder.linkTo(methodOn(EmployeeController.class).employee(employee.getId())).withSelfRel();
//            employee.add(selfLink);
//        });
        return employees;
    }


    @Override
    public EmployeeModel toModel(EmployeeEntity entity) {
        EmployeeModel employee = instantiateModel(entity);
        Link selfLink = WebMvcLinkBuilder.linkTo(methodOn(EmployeeController.class).employee(entity.getId())).withSelfRel();
        employee.add(selfLink);
        employee.setId(entity.getId());
        employee.setName(entity.getName());
        return employee;
    }
}
