package com.example.model.assembler;

import com.example.controller.DepartmentController;
import com.example.entity.DepartmentEntity;
import com.example.model.DepartmentModel;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class DepartmentModelAssembler extends RepresentationModelAssemblerSupport<DepartmentEntity, DepartmentModel> {

    public DepartmentModelAssembler() {
        super(DepartmentController.class, DepartmentModel.class);
    }

    @Override
    public CollectionModel<DepartmentModel> toCollectionModel(Iterable<? extends DepartmentEntity> entities) {
        CollectionModel<DepartmentModel> departments = super.toCollectionModel(entities);
        Link selfLink = WebMvcLinkBuilder.linkTo(methodOn(DepartmentController.class).departments()).withSelfRel();
        departments.add(selfLink);
        return departments;
    }


    @Override
    public DepartmentModel toModel(DepartmentEntity entity) {
        DepartmentModel department = instantiateModel(entity);
        Link selfLink = WebMvcLinkBuilder.linkTo(methodOn(DepartmentController.class).department(entity.getId())).withSelfRel();
        Link empLink = linkTo(methodOn(DepartmentController.class).employees(entity.getId())).withRel("employees");
        department.add(empLink);
        department.add(selfLink);
        department.setId(entity.getId());
        department.setName(entity.getName());
        return department;
    }
}
