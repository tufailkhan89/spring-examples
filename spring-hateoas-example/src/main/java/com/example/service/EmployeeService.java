package com.example.service;

import com.example.util.DataProvider;
import com.example.entity.EmployeeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    @Autowired
    private DataProvider dataProvider;

    public EmployeeEntity employee(String empId) {
        Optional<EmployeeEntity> employee = dataProvider.employee(empId);
        if (employee.isPresent()) {
            return employee.get();
        }
        throw new IllegalArgumentException("Employee with Id: [" + empId + "] not found");
    }
}
