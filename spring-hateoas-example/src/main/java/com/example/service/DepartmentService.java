package com.example.service;

import com.example.entity.DepartmentEntity;
import com.example.entity.EmployeeEntity;
import com.example.util.DataProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {

    @Autowired
    private DataProvider dataProvider;

    public List<DepartmentEntity> departments() {
        return dataProvider.departments();
    }

    public DepartmentEntity department(int deptId) {
        Optional<DepartmentEntity> department = dataProvider.department(deptId);
        if (department.isPresent()) {
            return department.get();
        }
        throw new IllegalArgumentException("Department with Id: [" + deptId + "] not found");
    }

    public List<EmployeeEntity> employees(int deptId) {
        Optional<DepartmentEntity> department = dataProvider.department(deptId);
        if (department.isPresent()) {
            return department.get().getEmployees();
        }
        throw new IllegalArgumentException("Department with Id: [" + deptId + "] not found");
    }
}
