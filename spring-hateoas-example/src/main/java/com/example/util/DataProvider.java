package com.example.util;

import com.example.entity.DepartmentEntity;
import com.example.entity.EmployeeEntity;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Component
public class DataProvider {

    List<DepartmentEntity> departments = new ArrayList<>();

    @PostConstruct
    public void loadData() {
        EmployeeEntity e2 = new EmployeeEntity("E2", "Virat");
        EmployeeEntity e4 = new EmployeeEntity("E4", "Rahul");
        EmployeeEntity e5 = new EmployeeEntity("E5", "Sachin");

        List<EmployeeEntity> hRDeptEmployees = Arrays.asList(e4, e5);
        List<EmployeeEntity> enggDeptEmployees = Arrays.asList(e2, e2);

        DepartmentEntity d1 = new DepartmentEntity(1, "Human Resource", hRDeptEmployees);
        DepartmentEntity d2 = new DepartmentEntity(2, "Engineering", enggDeptEmployees);

        departments.add(d1);
        departments.add(d2);
    }

    public List<DepartmentEntity> departments() {
        return departments;
    }

    public Optional<DepartmentEntity> department(int deptId) {
        return departments().stream().filter(dept -> dept.getId() == deptId).findFirst();
    }

    public List<EmployeeEntity> employees() {
        List<EmployeeEntity> employees = new LinkedList<>();
        departments().forEach(department -> {
            employees.addAll(department.getEmployees());
        });
        return employees;
    }

    public Optional<EmployeeEntity> employee(String empId) {
        return employees().stream().filter(employee -> employee.getId().equalsIgnoreCase(empId)).findFirst();
    }
}
