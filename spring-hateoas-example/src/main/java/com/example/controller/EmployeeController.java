package com.example.controller;

import com.example.entity.EmployeeEntity;
import com.example.model.EmployeeModel;
import com.example.model.assembler.EmployeeModelAssembler;
import com.example.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private EmployeeModelAssembler employeeModelAssembler;

    @GetMapping(value = "/employees/{empId}")
    public ResponseEntity<EmployeeModel> employee(@PathVariable(name = "empId") String empId) {
        EmployeeEntity employee = employeeService.employee(empId);
        return ResponseEntity.ok(employeeModelAssembler.toModel(employee));
    }
}
