package com.example.controller;

import com.example.entity.DepartmentEntity;
import com.example.entity.EmployeeEntity;
import com.example.model.DepartmentModel;
import com.example.model.EmployeeModel;
import com.example.model.assembler.DepartmentModelAssembler;
import com.example.model.assembler.EmployeeModelAssembler;
import com.example.service.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private DepartmentModelAssembler departmentModelAssembler;

    @Autowired
    private EmployeeModelAssembler employeeModelAssembler;

    @RequestMapping(value = "/departments")
    public ResponseEntity<Collection<DepartmentModel>> departments() {
        List<DepartmentEntity> departments = departmentService.departments();
        return ResponseEntity.ok(departmentModelAssembler.toCollectionModel(departments).getContent());
    }

    @GetMapping(value = "/departments/{deptId}")
    public ResponseEntity<DepartmentModel> department(@PathVariable(name = "deptId") int deptId) {
        DepartmentEntity department = departmentService.department(deptId);
        return ResponseEntity.ok(departmentModelAssembler.toModel(department));
    }

    @GetMapping(value = "/departments/{deptId}/employees")
    public ResponseEntity<Collection<EmployeeModel>> employees(@PathVariable(name = "deptId") int deptId) {
        List<EmployeeEntity> employees = departmentService.employees(deptId);
        return ResponseEntity.ok(employeeModelAssembler.toCollectionModel(employees).getContent());
    }

}
