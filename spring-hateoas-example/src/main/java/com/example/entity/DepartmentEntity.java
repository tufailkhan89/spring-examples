package com.example.entity;


import java.io.Serializable;
import java.util.List;

public class DepartmentEntity implements Serializable {
    private int id;
    private String name;

    List<EmployeeEntity> employees;

    public DepartmentEntity() {
    }

    public DepartmentEntity(int id, String name, List<EmployeeEntity> employees) {
        this.id = id;
        this.name = name;
        this.employees = employees;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EmployeeEntity> getEmployees() {
        return employees;
    }

    public void setEmployees(List<EmployeeEntity> employees) {
        this.employees = employees;
    }
}
