In order to understand **HATEOAS**, let us take an example of a website. 
As we know that a website is a collection of web pages of related content. 
These pages contain links to other pages, which helps users to navigate from one page to another without any additional information. 

These contents of a web page with links in it is also known as HyperText and the page itself is called a HyperText document.

Similarly the term “**hypermedia**” refers to any content that contains links to other forms of media such as images, movies, and text.


_Having said that, let us now define what HATEOAS is?_
“HATEOAS” is an Architectural style that takes the benefit of the above concept. 

`It allows the client to dynamically navigate to the appropriate resources by traversing the hypermedia links embedded in the response. 
`

_This Navigation through hypermedia links is conceptually the same as a user browsing web pages._ 

A HATEOAS based REST API not only provides the data that user requested but it also provides URI of actions that can be performed on the data. A Typical HATEOAS API response contains:- 
* Data that user requested
* A reference link to itself
* Reference(s) links to get action(s) that can be performed further on given data.


**One of the most important reasons for using HATEOAS is that it provides loose coupling between REST Service and its consumer**. 

A  consumer of a REST service now doesn’t need to hard-code all the resource URLs. It can get those URLs from the API response itself.

In this example, we can see how we can build HATEOAS based REST API using Spring Boot HATEOAS module.

Refer to Example Code for more details.