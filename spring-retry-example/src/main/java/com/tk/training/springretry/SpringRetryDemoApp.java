package com.tk.training.springretry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@EnableRetry
@SpringBootApplication
public class SpringRetryDemoApp {
    public static void main(String[] args) {
        SpringApplication.run(SpringRetryDemoApp.class, args);
    }
}
