package com.tk.training.springretry.controller;

import com.tk.training.springretry.model.Student;
import com.tk.training.springretry.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class StudentRestController {

    @Autowired
    private StudentService studentService;

    @Autowired
    private Environment environment;

    @GetMapping("/students")
    public ResponseEntity<List<Student>> simulateRetryAPI(@RequestParam(value = "simulateRetry", required = false,defaultValue = "false") Boolean simulateRetry) throws URISyntaxException {
        return ResponseEntity.ok(studentService.getStudents(simulateRetry));
    }
}
