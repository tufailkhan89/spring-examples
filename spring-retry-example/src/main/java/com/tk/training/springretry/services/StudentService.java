package com.tk.training.springretry.services;

import com.tk.training.springretry.model.Student;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Provides methods for handling Student related operations
 */
@Service
public class StudentService{

    /**
     * Returns list of students
     *
     * @param simulateRetry
     * @return
     */
    @Retryable(value = {RuntimeException.class}, maxAttemptsExpression = "#{@environment.getProperty('retry.attempt','2')}", backoff = @Backoff(delayExpression = "#{@environment.getProperty('retry.attempt.delay','1000')}", multiplierExpression = "#{@environment.getProperty('retry.attempt.delay.multiplier','3')}"))
    public List<Student> getStudents(final Boolean simulateRetry) {
        List students = new LinkedList();
        students.add(createStudent(1, "John"));
        students.add(createStudent(2, "Sanjay"));

        if (simulateRetry) {
            int randomNumber = new Random().nextInt();
            if (randomNumber % 2 != 0) {
                System.out.println("Simulating spring-retry by throwing RuntimeException");
                throw new RuntimeException("Throwing RuntimeException to simulate Spring Retry");
            }else if (randomNumber % 3 != 0) {
                System.out.println("Simulating spring-retry by throwing IllegalArgumentException");
                throw new IllegalArgumentException("Throwing IllegalArgumentException to simulate Spring Retry");
            }

        }
        return students;
    }


    /**
     * After all attempts to recover failed due to RuntimeException. This method will be called by SpringRetry Framework.
     *
     * @param exception Exception of type RuntimeException
     * @see RuntimeException
     */
    @Recover
    public List<Student> recover(RuntimeException exception,final Boolean simulateRetry) {
        System.out.println("All attempts to recover from failure due to RuntimeException are finished");
        System.out.println("Original input to the failed method was simulateRetry:"+simulateRetry);
        exception.printStackTrace();

        return new LinkedList<>();
    }

    /**
     * After all attempts to recover failed due to IllegalArgumentException. This method will be called by SpringRetry Framework.
     *
     * @param exception Exception of type IllegalArgumentException
     * @see RuntimeException
     */
    @Recover
    public List<Student> recover(IllegalArgumentException exception,final Boolean simulateRetry) {
        System.out.println("All attempts to recover from failure due to IllegalArgumentException are finished");
        System.out.println("Original input to the failed method was simulateRetry:"+simulateRetry);
        exception.printStackTrace();

        return new LinkedList<>();
    }

    /**
     * Create Sample student object
     *
     * @param rollNo
     * @param name
     * @return
     */
    private Student createStudent(int rollNo, String name) {

        Student student = new Student();
        student.setName(name);
        student.setRollNo(1);

        return student;
    }
}
