# README #

A Sample Application to demonstrate spring retry framework

### What is this repository for? ###

* Quick summary - A Sample Application to demonstrate spring retry framework
* Version - 1.0-SNAPSHOT
* Repository - https://bitbucket.org/tufailkhan89/spring-examples/src/master/spring-retry-example/

### How do I get set up? ###
* Run the below command to close the repository
```
    git clone git@bitbucket.org:tufailkhan89/spring-examples.git
```
* Configuration
<p>Refer to application.properties file for details.</p>

* Dependencies
    * Java8  
    * maven
* Deployment instructions
    * Execute below command to start the application 
    
```java
    mvn spring-boot:run
```

* Invoking Rest API
Once the application is started, Rest API can be invoked using below details
```text
GET - http://localhost:8080/api/students?simulateRetry=true
```  
**Note**
This API Implements random failure using a Random number generator. Inorder to examine different behaviour of the api, please invoke multiple times. 

### Example of different behaviour ###
#### When all attempt to recovery from Exception is exhausted

 
**API Response:-**
```json
[] 
```

 
**Exception Trace:-**

 ```java
Simulating spring-retry by throwing RuntimeException
Simulating spring-retry by throwing IllegalArgumentException
Simulating spring-retry by throwing RuntimeException
All attempts to recover from failure due to RuntimeException are finished
Original input to the failed method was simulateRetry:true
java.lang.RuntimeException: Throwing RuntimeException to simulate Spring Retry
	at com.tk.training.springretry.services.StudentService.getStudents(StudentService.java:35)
 ```
 
#### When API Recovered from initial failure during retry.
  
**API Response:-**
 
 ```json
 [{"name":"John","rollNo":1},{"name":"Sanjay","rollNo":1}] 
```
 

### Spring Retry Framework annoations ###
* @Retryable - Refer [Spring documentation](https://docs.spring.io/spring-retry/docs/api/current/org/springframework/retry/annotation/Retryable.html)
* @Recover -   Refer [Spring Documentation](https://docs.spring.io/spring-retry/docs/api/current/org/springframework/retry/annotation/Recover.html)